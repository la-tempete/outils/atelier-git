# Git et Gitlab

![Git](images/git.png)

`Git` est un système de **gestion de versions** pour enregistrer **l'évolution des fichiers**, des projets.

![Gitlab](images/gitlab-logo.svg), ![Github](images/github-logo.svg), ![Bitbucket](images/bitbucket-logo.svg) sont des plates-formes web d'hébergement de Git (serveur web de Git), avec beaucoup d'autres fonctionnalités.

| ![Gitlab](images/gitlab-icon.svg) | ![Github](images/github-icon.svg) | ![Bitbucket](images/bitbucket-icon.svg) |
| --------------------------------- | --------------------------------- | --------------------------------------- |
| 2012                              | 2008                              | 2011                                    |
| ouvert                            | propriétaire                      | propriétaire                            |
| service et installation           | service                           | service                                 |
| gratuit par une personne          | gratuit par une personne          | gratuit par une équipe de 5 personnes   |
| avec projets privés et publics    | avec projets publics              | avec projets privés et publics          |

## GitLab

Le GitLab donne la possibilité, du fait de l'ouverture de son code source, d'héberger un serveur web de Git indépendant. Il y a, par exemple, des instances chez Framasoft et chez Inria - L'Institut national de recherche en informatique et en automatique.

### ![Framasoft](images/framasoft-logo.svg)

> Un réseau dédié à la promotion du « libre » en général et du logiciel libre en particulier.
>
> De nombreux services et projets innovants mis librement à disposition du grand public.
>
> Une [communauté](https://framasoft.org/fr/full/#topPgCommunaute) de bénévoles soutenue par une association d'intérêt général.
>
> Une invitation à bâtir ensemble un monde de partage et de coopération.

[Framagit](https://framagit.org) :
> Une instance de l'édition communautaire de Gitlab proposée par Framasoft dans le cadre de son opération « Dégooglisons Internet » et est soumise à acceptation des [Conditions Générales d'Utilisations](https://framasoft.org/fr/cgu/).
>
> Un outil qui s'adresse aux développeurs, développeuses, mais aussi, aux contributeurs et contributrices.
>
> Il permet d'héberger nos projets de logiciels libres et de travailler collaborativement dessus.

Les [projets publics sur Framagit](https://framagit.org/public/projects).

Les projets privés sont disponibles seulement pour les membres de chaque projet.

### ![Inria](images/inria-logo.svg)

Les [projets publics sur Inria](https://gitlab.inria.fr/explore/projects). Cette instance de GitLab offre seulement le service d'hébergement de projets développés par les équipes de l'Inria.
