# Atelier Git

:zap: À trois pas de l'aisance numérique !

![Git](images/git.png)

## But
Pratiquer la **gestion de versions** et poursuivre **l'évolution d'un projet**
avec les concepts de Git.

## Prérequis
- :question: Courage :exclamation:
- :computer: Ordinateur avec internet :globe_with_meridians:;
-  Navigateur, terminal et éditeur de texte (existent déjà par défaut).

## Sommaire
Les étapes suivantes peuvent être consultées à n'importe quel moment et de façon
indépendante !

- [:checkered_flag: On y va !](pratique.md)

- [:feet: Premiers pas avec le terminal](terminal.md)

- [:seedling: Git et GitLab](git-gitlab.md)

- [:pencil: Textes avec Markdown](markdown.md)

- [:bookmark_tabs: References](references.md)


---
