# :bookmark_tabs: References

## Terminal

[Guide d’autodéfense numérique : Utiliser un terminal](https://guide.boum.org/tomes/1_hors_connexions/3_outils/00_utiliser_un_terminal/)

## Git

#### Formation / Tutoriel
[Formation « Comprendre Git » chez Grafikart](https://www.grafikart.fr/tutoriels/git-presentation-1090)

#### Planche
[git cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

### Guide
[git - la guía sencilla](http://rogerdudler.github.io/git-guide/index.es.html)

[git - guia prático](http://rogerdudler.github.io/git-guide/index.pt_BR.html)

[git - petit guide](http://rogerdudler.github.io/git-guide/index.fr.html)

[git - Der einfache Einstieg](http://rogerdudler.github.io/git-guide/index.de.html)

[git - the simple guide](http://rogerdudler.github.io/git-guide/index.html)

#### Livre
[Libro: español](https://git-scm.com/book/es/v2/)

[Livro: português (Brasil)](https://git-scm.com/book/pt-br/v2)

[Livre : français](https://git-scm.com/book/fr/v2)

[Buch: deutsch](https://git-scm.com/book/de/v2/)

[Book: english](https://git-scm.com/book/en/v2/)

---
[:leftwards_arrow_with_hook: Voltar ao README ](README.md)