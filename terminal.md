﻿# Premières pas avec le terminal

## Ouvrir le terminal
![Terminal](images/terminal-icon.png)

### Comment ouvrir un terminal sur :
[:penguin: linux](https://fr.wikihow.com/ouvrir-une-fen%C3%AAtre-de-terminal-dans-Ubuntu)

[:apple: mac](https://fr.wikihow.com/ouvrir-le-Terminal-sur-un-Mac)

[:bug: windows](https://fr.wikihow.com/ouvrir-l%27Invite-de-commandes-sous-Windows)

Avec un lien avec les équivalences entre les [commandes windows et linux](https://skimfeed.com/blog/windows-command-prompt-ls-equivalent-dir/)

## Vue initiale
Sur l'image ci-dessous on voit :
```bash
andre@rocha:~$
```
![Vue initiale](images/terminal-home.png)

### Details
On a sis éléments avec la logique suivante :

Un utilisateur (avec son **identifiant**) est connecté à une **machine**. Dedans cette machine, est affiché : le chemin du **dossier courant**. Après ce chemin il y a un caractère qui indique le **type de l'utilisateur** commun ou administrateur.


`IDENTIFIANT`@`LE NOM DE LA MACHINE`:~$

<details>
  <summary><mark>Plus de details (appuie ici pour afficher/masquer)</mark></summary>

`IDENTIFIANT` : andre

`@` : dedans / connecté

`LE NOM DE LA MACHINE` : rocha

`~` : surnom du dossier défaut de l'utilisateur.

`$` : utilisateur ordinaire / commun.

`#` : administrateur / super utilisateur.

#### Pour afficher le chemin complet
Écrire la commande `echo` sur le terminal :
```bash
echo ~
```

Résultat :
```bash
/home/andre
```
</details>

## Des commandes basiques

`ls` : « **l**i**s**t », pour lister les informations des fichiers (du répertoire / dossier courant par défaut).

`clear` : pour nettoyer l'écran du terminal, afin d'avoir un écran plus propre.

`cd` : « **c**hange **d**irectory », pour changer de répertoire / dossier.
  > Il existe deux dossiers spéciaux : le dossier `.` (référence pour le dossier courant) et le dossier `..` (référence pour le dossier antérieur ou qui contient le dossier courant).

`mkdir` : « **m**a**k**e **dir**ectory », pour créer un répertoire.

`mv` : « **m**o**v**e », pour déplacer des fichiers ou pour renommer un fichier/répertoire.

`cp` : « **c**o**p**y », pour copier des fichiers.

`exit` : pour quitter le terminal.


### :warning: les commandes suivantes sont irréversibles donc soyez vigilant en utilisant :

`rm` : « **r**e**m**ove », pour supprimer des fichiers/répertoires (avec l'option -r ou --recursive).

`rmdir` : « **r**e**m**ove **dir**ectory », pour supprimer des répertoires vides.


Pour plus d'information il y a la commande `man` qui affiche le manuel de une commande donnée comme argument, `mkdir` par exemple :
```bash
 man mkdir
```

<details>
  <summary><mark>Plus de details (appuie ici pour afficher/masquer)</mark></summary>

#### Structure des commandes

Toutes les commandes suivent une syntaxe paraît :

```bash
 ls          -l       --all      /home
 ^^          ^^       ^^^^^      ^^^^^
commande   option    option     argument
```

Avec l'option `--help` on peut savoir à quoi sert la **commande**, comment l'utiliser et les autres options disponibles.
```bash
 ls --help
```
Quelques option ont des format court et long comme `-a` et `--all`.

Pour afficher tous les fichiers du répertoire courant (aussi les fichiers cachés). Les fichiers cachés commence par un point (par exemple `.notes.txt`).
```bash
ls -a
ls --all
```

Pour afficher les fichiers du répertoire courant (sauf les fichiers cachés) en forme liste et les tailles des fichiers avec les préfixes du Système international d'unités comme : `1K`, `234M`, `2G`.
```bash
ls -l -h
ls -lh
ls -hl
ls -l --human-readable
ls --human-readable -l
```

L'ordre des options ne change pas le résultat, mais attention aux options avec arguments, car le changement de l'ordre doit contenir l'option et aussi ces arguments.

### :exclamation: Syntaxe
 **Chaque parties** de la commande est toujours **délimitée par un/des espace vide** (touche espace). Donc sans espace pour la délimitation, le terminal ne va pas comprendre la commande comme on voulait. Dans l'exemple ci-dessous le terminal ne va pas reconnaître la commande `ls`, ni les options ou argument.

 ```bash
 ls-l-h/home
 ^^^^^^^^^^^
  commande
```
</details>