# :checkered_flag: On y va !

Pour pratiquer la **gestion de versions**, on va utiliser `Git` *localement*
(sur son ordinateur) et à *distance* (sur une plate-forme / service web).

[:mag: Plus de détails à propos de Git](git-gitlab.md)

# 0. Préparation

## Localement :computer:

- Ouvrir un terminal;

  [:mag: Plus de détails à propos du Terminal](terminal.md).

- Vérifier si Git est déjà installé :
  ```bash
  git --version
  ```

  Si Git est installé, sera afficher le nom `git` avec la version actuelle, par exemple :
  ```bash
  git version 2.20.1
  ```

  Si Git ne pas encore installé, il faut l'installer.
  - Installer Git;

    [:mag: Plus de détails sur « git - petit guide »](http://rogerdudler.github.io/git-guide/index.fr.html).

## À distance :globe_with_meridians:

- Ouvrir un navigateur web;
- Créer une compte sur [Framagit](https://framagit.org/) ou à travers du [lien direct](https://framagit.org/users/sign_in?redirect_to_referer=yes#register-pane);
    <details>
    <summary><mark>Plus de details (appuie ici pour afficher/masquer)</mark></summary>
    Pour s'identifier ou pour créer un compte, appuyez sur le bouton <code>s'identifier / s'inscrire</code> sur l'écran en haut à droite :
    <img src="images/framagit-signin-register.png" title="Framagit s'identifier / s'inscrire" alt="Framagit s'identifier / s'inscrire">

    Pour s'inscrire, sélectionner l'onglet `Register` :
    <img src="images/framagit-register.png" title="Framagit s'inscrire" alt="Framagit s'inscrire">

    Confirmer les coordonnées de votre compte depuis un émail qui vous sera envoyé;
    </details>

- [Ajouter la clé SSH](https://framagit.org/profile/keys) dans le profile;

# 1. Cloner un dépôt

## À distance :globe_with_meridians:

- Accéder, avec le navigateur, à la page d'accueil du projet;
  >  Exemple : `https://framagit.org/la-tempete/outils/atelier-git`

- Appuyer sur le bouton `Clone` en haut à droite;
    <details>
    <summary><mark>Plus de details (appuie ici pour afficher/masquer)</mark></summary>
    <img src="images/atelier-git-clone-button.png" title="Le bouton clone" alt="Le bouton clone">

    Sera affiché les deux options, `Clone with SSH` et `Clone with HTTPS`
    <img src="images/atelier-git-clone.png" title="Les options du clone" alt="Les options du clone">
    </details>

- Copier l'URL de l'option `Clone with SSH`
    > `git@framagit.org:la-tempete/outils/atelier-git.git`

## Localement :computer:

- Choisir un répertoire/dossier où sera créé le dépôt du projet « Atelier Git »;

    Par exemple :

    :penguin: linux
    ```bash
    cd /home/andre/
    ```

    :apple: mac
    ```bash
    cd /Users/andre/
    ```

    :bug: windows
    ```bash
    cd C:\Utilisateurs\andre
    ```

    Si le dossier n'existe pas, il faut le créer, avec la commande `mkdir` (:penguin: linux et :apple: mac) ou `md` (:bug: windows) par exemple.

    [:mag: Plus de détails à propos du Terminal](terminal.md).

- Cloner le dépot du projet, en écrivant sur le terminal  :
    ```bash
    git clone git@framagit.org:la-tempete/outils/atelier-git.git
    ```

    > À l'intérieur du dossier actuel (celui qu'on a choisi au juste avant) sera créé le dossier `atelier-git`, avec les fichiers du projet.
    >
    > Le dossier `atelier-git` contiendra un dossier caché `.git`. Ce dossier garde les informations utilisées par la commande `git` pour faire le contrôle des versions.

- Changer de dossier, pour accéder au projet, avec la commande :
    ```bash
    cd atelier-git
    ```

# 2. Créer une branche

- D'abord mettre à jour le dépôt :
    ```bash
    git checkout master
    git pull --rebase
    ```

    > La branch principale par défaut peut avoir d'autre nom, comme `develop`, donc il fault faire le `checkout` avec ce nom, par exemple :
    > ```bash
    > git checkout develop
    > ```

- Créer une branche :

    Les branches permettent un travail en parallèle et sont utilisées pour développer ou corriger des fonctionnalités isolées des autres. Le nom de la branche doit décrit na tâche et/ou le nom de la personne en question, `translation-es-git-gitlab` par exemple pour la traduction en espagnol du texte sur Git et Gitlab pour :
    ```bash
    git checkout -b translation-git-gitlab-es
    ```

    Cette nouvelle branche sera un copie de la branche courante au moment de sa création.

# 3. Faire des changements

- Éditer le fichier `git-gitlab.md` et traduire la prèmier ligne du français vers l'espagnol et sauvegarder;
    > La version en espagnol du livre de Git peut être utile, [https://git-scm.com/book/es/v2](https://git-scm.com/book/es/v2)

- Vérifier les changements :
    ```bash
    git status
    ```

- Montrer les changements :
    ```bash
    git diff git-gitlab.md
    ```

# 4. Faire des changements

- Ajouter les changements du fichier :
    ```bash
    git add git-gitlab.md
    ```

> Répéter les items 3 et 4 jusqu'à finir la traduction ou le période de travail.

# 5. Créer une version

- D'abord régarder les changements du fichier :
    ```bash
    git status
    ```

- Valider ces changements :
    ```bash
    git commit
    ```

> Écrire un **message** descriptif des changements et leur contexte.
>
> *Ce message est très important*, lire l'article [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/) sur comment écrire un bon message.

# 6. Envoyer les changements à le dépôt distant

- D'abord configurer les donnes du utilisateur André, par exemple :
    ```bash
    git config --global user.name "Andre Rocha"
    git config --global user.email rocha.matcomp@gmail.com
    ```

- Envoyer les changements
    ```bash
    git push --set-upstream origin translation-git-gitlab-es
    ```

    > Cette commande définit la branche distante correspondent et aussi envoie les fichier de la version donne par le commit.
